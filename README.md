# Async Api

## FastAPI, Elasticsearch, Nginx

### Get starter   
_____________________
```bash
git clone https://gitlab.com/kinozal/async_api.git
cd async_api
docker-compose up --build -d
```

В результате должны подняться контейнеры:
 - elasticsearch
 - redis
 - nginx
 - api

Также при страте контейнера с api создаются индексы, \
возможно правильней перенести скрипт создания индексов в entrypoint.sh

Активировать виртуальное окружение можно при помошьи poetry или venv 
для этого перейти в папку `src` и активировать виртуально окружение 


### Dev 
__________________
**poerty:** 
```bash
cd src 
poetry shell
poetry install
```
**venv:**
```bash
cd src
python -m venv venv
venv\Scripts\activate
```

Также в файле .env необходим изменить хосты с имен контейнеров на localhost
 - REDIS_HOST = localhost
 - ELASTIC_HOST = localhost

При создании sh файлов в windows получатся не приятность так ее можно пофиксить:
```dos2unix.exe entrypoint.sh```